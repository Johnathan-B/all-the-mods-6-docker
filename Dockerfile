# Johnathan Burns
# 2020-08-26
# Dockerfile to pull and run the current All the Mods 6 server

FROM debian:latest

# Install requisite software
RUN apt-get update && apt-get install -y \
	openjdk-11-jre-headless \
	wget

# Add a user to run the server
RUN ["useradd", "minecraftforge", "-m", "-s", "/bin/false"]
USER minecraftforge

# Copy in the (patched) setup files and extract to the home dir
COPY ATM6-0.29u.tar /tmp
RUN ["tar", "xf", "/tmp/ATM6-0.29u.tar", "-C", "/home/minecraftforge"]

# Make the script executable
WORKDIR /home/minecraftforge/ATM6
RUN ["chmod", "+x", "startserver.sh"]

# Configure and run 
EXPOSE 25565/tcp
ENTRYPOINT ["./startserver.sh"]
