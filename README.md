Docker image for an All the Mods 6 Server
---

To build:
Run `docker build -t atm6 .`

To run:
`docker run --name [SERVERNAME] --restart unless-stopped -d -t -p 25565:25565/tcp atm6:latest`
Creates a container named [SERVERNAME] from the image created above, with the correct port exposed, that will automatically restart with a system reboot.
